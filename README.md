# Construtodo

Mobile application for a building company MIS.

**NOTE: Original source code is private, this repository is only intented for demonstration purposes.**

## Technical information

* **Framework**: [Ionic](https://ionicframework.com/) v3.6.1
* **Libraries**: 
  * [Dragula](https://github.com/bevacqua/dragula)
  * [RxJS](https://rxjs-dev.firebaseapp.com/)
  * [Cordova integrations](https://ionicframework.com/docs/native/community)
## About my contribution

I mainly developed the devolutions module for digital invoices. It consisted on searching invoices by number or customer name then selecting the products to return using a drag & drop feature. The products are dragged into a devolution cart 
zone and then dropped. A single devolution cart may have products for more than an invoice (all belonging to the same customer). After all products are selected there is a devolution detail page with all the products, quantities and amount; the quantities of each product can be edited if it's needed to do a partial devolution. The user can leave the page in any time and return when he needs and the cart info must persist until he does. When the user is sure with the devolution, he emit it and then the system creates and show a devolution ticket.

Beside this module, I made another fixes on layouts design, navigation and code formatting.


![Login Interface](img/1.png?raw=true) 
![Home](img/2.png?raw=true)  
![Reports menu](img/3.png?raw=true) 
![Sales list](img/4.png?raw=true)  
![Invoice](img/5.png?raw=true) 
![Products](img/6.png?raw=true)
